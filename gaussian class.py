#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from numpy import ones,copy,cos,tan,pi,linspace,exp

class gaussian:
    def __init__(self, N, a, b, x):
        self.iterations = N
        self.lower_limit = a
        self.upper_limit = b
        self.function_to_integrate = x
        self.epsilon = 1e-15
        self.delta = 1.0
        self.polynomial = 0
    def gaussxw(self, N):

        # Initial approximation to roots of the Legendre polynomial
        y = linspace(3,4*N-1,N)/(4*N+2)
        self.polynomial = cos(pi*y+1/(8*N*N*tan(y)))


        # Find roots using Newton's method
    #     epsilon = 1e-15
    #     delta = 1.0
        while self.delta>self.epsilon:
            p0 = ones(N,float)
            p1 = copy(self.polynomial)
            for k in range(1,N):
                p0,p1 = p1,((2*k+1)*self.polynomial*p1-k*p0)/(k+1)
            dp = (N+1)*(p0-self.polynomial*p1)/(1-self.polynomial*self.polynomial)
            dx = p1/dp
            self.polynomial -= dx
            self.delta = max(abs(dx))

        # Calculate the weights
        w = 2*(N+1)*(N+1)/(N*N*(1-self.polynomial*self.polynomial)*dp*dp)

        return self.polynomial,w

    def gaussxwab(self, N,a,b):
        self.polynomial,w = gaussxw(N)
        return 0.5*(b-a)*self.polynomial+0.5*(b+a),0.5*(b-a)*w
    
    def integral(self, N, a, b):
        points, weights = gaussxw(N)
        scaled_points, scaled_weights = gaussxwab(N,a,b)

        integrated_function = 0
        for i,j in enumerate(scaled_points):
            integrated_function += w(j) * scaled_weights[i]
        return integrated_function


#defining the function we are trying to take 

